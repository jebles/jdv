/** 
Proyecto: Juego de la vida.
 * Implementa el almacén de datos del programa. 
 * @since: prototipo1.1
 * @source: Datos.java 
 * @version: 1.2 - 14.2.19
 * @coauthor: ajp 
 * @coauthor: dam18-23jpr 4004010
 */

package accesoDatos;

import java.util.ArrayList;
import java.util.HashMap;
import modelo.*;
import modelo.Usuario.RolUsuario;
import util.Fecha;

public class Datos {

	// entr < > ponemos lo que queremos que se asegure que el contenido sean objetos usuario (~infer)
	private static ArrayList <Usuario> datosUsuarios = new ArrayList <Usuario>();
	private static ArrayList <SesionUsuario> datosSesiones = new ArrayList <SesionUsuario>();
	private static ArrayList <Simulacion> datosSimulaciones = new ArrayList <Simulacion>();
	private static ArrayList<Mundo> datosMundos = new ArrayList<Mundo>();
	private static HashMap<String, String> mapaEquivalencias = new HashMap<String, String>(); 

	/**
	 * @param clave
	 * @return el id que corresponde a una clave
	 */
	public String getEquivalenciaId(String clave) {
		return mapaEquivalencias.get(clave);
	}
	public int getSesionesRegistradas() {
		return datosSesiones.size();
	}
	public int getUsuariosRegistrados() {
		return datosUsuarios.size();
	}
	public int getSimulacionesRegistradas() {
		return datosSimulaciones.size();
	}

	/**
	 * buscarusuario por su id, base 1
	 * @param id
	 * @return Usuario
	 * 
	 */
	public Usuario buscarUsuario(String idUsr) {
		int indice = indexSort(idUsr);
		if (indice < 0) {
			mapaEquivalencias.get(idUsr);
		}
		return datosUsuarios.get(indice - 1 ); // base 1

	}
	/**
	 * Busca sesionUsaurio dado su id.
	 * @param idSesion 
	 * @return - la SesionUsuario encontrada o null si no existe.
	 */
	public SesionUsuario buscarSesion(String idSesion) {
		// Busca sesionUsuario coincidente con la credencial.
		for (SesionUsuario sesion : datosSesiones) {
			if (sesion != null
					&& sesion.getIdSesion().equalsIgnoreCase(idSesion)) {
				return sesion;	// Devuelve la sesionUsuario encontrada.
			}
		}
		return null;			// No encuentra.
	}
	/**
	 * Busca usuario dado su id.
	 * El indice es en base 1
	 * basado en busqueda binaria
	 * @param idUsr - el id del Usuario a buscar.
	 * @return - el Usuario encontrado o el indice negativo de su posición teórica
	 */
	public int indexSort(String idUsr) {		

		int medio = 0;
		int inf = 0;
		int sup = datosUsuarios.size() - 1;

		while(inf <= sup){
			medio = (sup + inf) / 2;
			Usuario med = datosUsuarios.get(medio);
			String idMed = med.getId();
			int comparacion = idUsr.compareTo(idMed);
			
			if(comparacion == 0) {
				return medio + 1;
			}
			if (comparacion > 0) {
				inf = medio + 1;
			}
			else {
				sup = medio - 1;
			}
		}
		return -(inf + 1);
	}	
	/**
	 * Busca simulación dado su id.
	 * @param idSesion 
	 * @return - la simulacion encontrada o null si no existe.
	 */
	public Simulacion buscarSimulacion(String idSimulacion) {
		// Busca simulacion coincidente con la credencial.
		for (Simulacion simulacion : datosSimulaciones) {
			if (simulacion != null
					&& simulacion.getIdSimulacion().equalsIgnoreCase(idSimulacion)) {
				return simulacion;	// Devuelve la simulación encontrada.
			}
		}
		return null;				// No encuentra.
	}
	
	/**
	 * Registro de la sesión de usuario.
	 * @param sesionUsuario 
	 */
	public void altaUsuario(Usuario usr) {
		// Registra usuario a partir de la última posición ocupada.
		assert usr != null;
		int posicionInsercion = indexSort(usr.generarId());
		//si el usuario no se encuentra (indice positivo)
		if (posicionInsercion > 0) {
			datosUsuarios.add(-posicionInsercion -1, usr);
			registrarEquivalencias(usr);
		
		} else {
			// Coincidencia de id
			
			// Error usr repetido
		}
	}

	/**
	 * Muestra por consola todos los usuarios almacenados.
	 */
	public void mostrarTodosUsuarios() {
		for (Usuario usr: datosUsuarios) {
			if (usr == null) {
				break;
			}
			System.out.println("\n" + usr);			
		}
	}

	/**
	 * Genera datos de prueba válidos dentro 
	 * del almacén de datos.
	 */
	public void cargarUsuariosPrueba() {	
		for (int i = 0; i < 10; i++) {
			altaUsuario(new Usuario(new Nif("0000000" + i + "TRWAGMYFPDXBNJZSQVHLCKE".charAt(i)), 
					"Pepe", "López Pérez", 
					new DireccionPostal("Luna", "27", "30132", "Murcia"),
					new Correo("pepe" + i + "@gmail.com"), 
					new Fecha(1999, 11, 12), 
					new Fecha(2018, 01, 03), 
					new ClaveAcceso("Miau#" + i), RolUsuario.NORMAL));
		}
	}

	/**
	 * Registro de la sesión de usuario.
	 * @param sesionUsuario 
	 */
	public void altaSesion(SesionUsuario sesion) {
		datosSesiones.add(sesion);  
	}

	/**
	 * Registro de la simulación.
	 * @param simulacion 
	 */
	public void altaSimulacion(Simulacion simulacion) {
		datosSimulaciones.add(simulacion);  
	}
	/**
	 * Registro de las equivalencias de la tabla nif-id de usuarios
	 * @param usuario 
	 */

	private void registrarEquivalencias(Usuario usr) {
		mapaEquivalencias.put(usr.getNif().getTexto(), usr.getId());
		mapaEquivalencias.put(usr.getCorreo().getTexto(), usr.getId());
		mapaEquivalencias.put(usr.getId(), usr.getId());
	}
	

} // class
