/** 
Proyecto: Juego de la vida.
 * Implementa los mecanismos de interaccion con el usuario. 
 * @since: prototipo1.1
 * @source: Presentacion.java 
 * @version: 1.26 - 7.3.19 - gestion de los errores
 * @author: ajp
 * @coauthor: jpr - 4004010
 */

package accesoUsr;

import java.util.Scanner;

import accesoDatos.Datos;
import accesoDatos.DatosException;
import modelo.ClaveAcceso;
import modelo.ModeloException;
import modelo.Simulacion;
import modelo.Usuario;
import util.Fecha;

public class Presentacion {

	public static final int MAX_INTENTOS_FALLIDOS = 3;
	private Usuario usrEnSesion;
	private static Datos datos;
	private Simulacion simulacion;

	public Usuario getUsrEnSesion() {
		return this.usrEnSesion;
	}
	public Simulacion getSimulacionn() {
		return this.simulacion;
	}
	public Presentacion() throws DatosException, ModeloException {
		datos = new Datos();
	}
	
	
	/**
	 * Despliega en la consola el estado almacenado, corresponde
	 * a una generación del Juego de la vida.
	 */
	public void mostrarSimulacion() {
		int generacion = 0;
		
		for (int i = 0; i < simulacion.getMundo().getEspacio().length; i++) {
			for (int j = 0; j < simulacion.getMundo().getEspacio().length; j++) {
				System.out.print((simulacion.getMundo().getEspacio()[i][j] == 1) ? "|o" : "| ");
			}
			System.out.println("|");
		}
	}

	/**
	 * Controla el acceso de usuario.
	 * @return true si la sesión se inicia correctamente.
	 */
	public boolean inicioSesionCorrecto() {
		Scanner teclado = new Scanner(System.in);	// Entrada por consola.
		int intentosPermitidos = MAX_INTENTOS_FALLIDOS;

		do {
			// Pide usuario y contraseña.
			System.out.print("Introduce el ID de usuario: ");
			String id = teclado.nextLine().toUpperCase();
			System.out.print("Introduce clave acceso: ");
			ClaveAcceso clave = null;

			try {
				clave = new ClaveAcceso();
				clave = new ClaveAcceso(teclado.nextLine());			

				// Busca usuario coincidente con las credenciales.
				
				String idequivalente = datos.getEquivalenciaId(id);
				
				usrEnSesion = datos.buscarUsuario(idequivalente);
			System.out.println(usrEnSesion);
				//System.out.println("encontrado " + usrEnSesion.getId().toString());

				// Encripta clave tecleada utilizando un objeto temporal
				// que ejecutará automáticamente el método privado.
				Usuario aux = new Usuario();
				aux.setClaveAcceso(new ClaveAcceso(clave));
				clave = aux.getClaveAcceso();
				System.out.println("la clave es "+clave);
			}
			catch (ModeloException e) {
				System.out.println("excepcion:"+e.getMessage());
				
			}
			if (usrEnSesion != null && usrEnSesion.getClaveAcceso().equals(clave)) {
				simulacion = datos.getSimulacion("III1R-" 
							+ new Fecha (0001,01, 01, 01, 01, 01).toStringMarcaTiempo() 
							);
				return true;
			} 
			else {
				intentosPermitidos--;
				System.out.print("Credenciales incorrectas: ");
				System.out.println("Quedan " + intentosPermitidos + " intentos... ");
			}

		} while (intentosPermitidos > 0);

		return false;
	}

} //class
