/** 
 *  Proyecto: Juego de la vida.
 *  Excepcion para la gestión de errores de Datos  
 *  @since: prototipo1.2
 *  @source: ModeloException.java 
 *  @version: 1.25 - 5.3.19 
 *  @author: ajp
 *  @coauthor: 4004010
 */
package accesoDatos;

public class DatosException extends Exception {

	public DatosException(String mensaje) {
		super(mensaje);
	}
	public DatosException() {
		super();
	}	

} //class
