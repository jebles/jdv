package accesoDatos;

import java.util.List;

/**
 * especifica las operaciones básicas del DAO
 * DAM1 
 * @author: dam18-23 - 4004010
 * @since:2.0
 * @source: OperacionesDAO.java
 * @version: 21 mar. 2019 
 */
public interface OperacionesDAO {
	
	/**
	 * Método obtener(String id) recibe un argumento que representa el 
	 * @param id por el que se ordenan los objetos de un mismo DAO..
	 * @return el objeto buscado o null si no lo encuentra
	 */
	
	Object obtener(String id);
	/**
	 * @return un objeto List con todos los el objeto del mismo tipo asociados al DAO.
	 */
	 List  obtenerTodos();
	 
	 /**
	  * devuelve el tamaño del objeto
	  */
	int getSize();
	
	/**
	 * recibe un argumento que representa el nuevo objeto a almacenar. 
	 * Lanza DatosException si ya existe.
	 * @throws DatosException 
	 */
	void alta(Object obj) throws DatosException;
	
	/**
	 * recibe un argumento que representa el id  
	 * Lanza DatosException si no existe.
	 * @param id por el que se identifica el objeto que se quiere borrar. 
	 * @return el objeto borrado.
	 */
	Object baja(String id)throws DatosException;;
	/**
	 * Lanza DatosException si no existe.
	 * @param obj - recibe un argumento que representa el objeto con las modificaciones a actualizar. 
	 * @throws DatosException 
	 */
	void actualizar(Object obj) throws DatosException;
	
	/**
	 * para poder mostrar todos los almacenados en el sistema asociados al mismo DAO. 
	 * @return el texto con los datos.
	 */	
	String listarDatos();
	/**
	 * mostrar todos los identificadores de los objetos almacenados en el sistema asociados al mismo DAO. 
	 * @return el texto con los datos.
	 */
	String listarId();
	
	void borrarTodo();
	
	/**
	 * patrón plantilla, cierra almacenes de datos
	 */
	default void cerrar() {
		
	}

}
