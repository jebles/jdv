package accesoDatos.fichero;

/**
 * Operaciones básicas de persistencia 
 * @author: dam18-23 - 4004010
 * @since: 2.0
 * @source: Persistente.java
 * @version: 29 mar. 2019 
 */
public interface Persistente {
	
	void guardarDatos();
	
	void cargarDatos();
	
	void recuperarDatos();

}
