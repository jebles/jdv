package accesoDatos.fichero;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import modelo.*;
import modelo.Simulacion.EstadoSimulacion;
import util.Fecha;

public class SimulacionesDAO extends DAOIndexSort implements OperacionesDAO{

	private static List<Identificable> datosSimulaciones;
	private static SimulacionesDAO instance;

	SimulacionesDAO() {
		datosSimulaciones = new ArrayList<Identificable>();
	}
	public static SimulacionesDAO getInstance() {
		if (instance == null ) {
			instance = new SimulacionesDAO();
		}
		return instance;
	}
	/** 
	 * Generar datos predeterminados
	 */
	
	private void cargarPredeterminados() {
		try {
			Usuario usrDemo = UsuariosDAO.getInstance().obtener("AAA1R");
			Mundo mundoDemo = (Mundo) MundosDAO.getInstance().obtener("Demo");
			
			alta(new Simulacion 
					(usrDemo, new Fecha (0001 , 01, 01, 01, 01, 01), 
						mundoDemo, 
						EstadoSimulacion.PREPARADA)
					);			
		}
		catch (DatosException | ModeloException e){			
		}
	}
//	/**
//	 * Busca usuario dado su id.
//	 * El indice es en base 1
//	 * basado en busqueda binaria
//	 * @param idUsr - el id del Usuario a buscar.
//	 * @return - el Usuario encontrado o el indice negativo de su posición teórica
//	 */
//	public int indexSort(String idSim) {		
//
//		int medio = 0;
//		int inf = 0;
//		int sup = datosSimulaciones.size() - 1;
//
//		while(inf <= sup){
//			medio = (sup + inf) / 2;
//
//			int comparacion = idSim.compareTo(datosSimulaciones.get(medio).getIdSimulacion());
//
//			if(comparacion == 0) {
//				return medio + 1;
//			}
//			if (comparacion > 0) {
//				inf = medio + 1;
//			}
//			else {
//				sup = medio - 1;
//			}
//		}
//		return -(inf + 1);
//	}	

	@Override
	public int getSize() {
		return datosSimulaciones.size();
	}

	@Override
	public Object obtener(String id) {
		assertNotNull(id);
		

		int posicion;
		if (id != null) {
			posicion = indexSort(id, datosSimulaciones) - 1; // base 1
			if (posicion < 0) {
				return (Simulacion) datosSimulaciones.get(posicion-1); // base 1
			}			
		}		
		return null;
	}

	@Override
	public List obtenerTodos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void alta(Object obj) throws DatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public Object baja(String id) throws DatosException {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * Actualiza datos de una simulacion reemplazando el almacenado po el recibido
	 * no tienen sentido cambios en usr ni en la fecha
	 * @param obj . patron con las modificaciones
	 * @throws DatosException - si no existe
	 */
	@Override
	public void actualizar(Object obj) throws DatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public String listarDatos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listarId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarTodo() {
		// TODO Auto-generated method stub
		
	}




}
