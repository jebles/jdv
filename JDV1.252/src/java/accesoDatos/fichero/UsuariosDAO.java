package accesoDatos.fichero;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import modelo.ClaveAcceso;
import modelo.Correo;
import modelo.DireccionPostal;
import modelo.Identificable;
import modelo.ModeloException;
import modelo.Nif;
import modelo.Usuario;
import modelo.Usuario.RolUsuario;
import util.Fecha;
/**
 * Resuelve el acceso a los datos de los usuarios incluyendo los mecanismos necesarios: 
 * Implementación de la persistencia mediante dos ficheros (arraylist para datos normales y otro para el map)
 * @author: dam18-23 - 4004010 - jpr
 * @since: 2.5
 * @source: UsuariosDAO.java
 * @version: 29.3.19
 */
public class UsuariosDAO extends DAOIndexSort implements OperacionesDAO, Persistente {

	//Singleton
	private static UsuariosDAO instance;

	//Elementos de almacenamiento
	private static Map <String, String> equivalenciasId ; //Map equivale a hastable
	private static List <Identificable> datosUsuarios;
	private File fUsuarios;
	private File fEquivalencias;

	//att:contructor privado de uso interno de la clase para inicializar solo una vez. 
	//TODO quitar excepciones de constructores 
	private UsuariosDAO() throws DatosException, ModeloException {
		equivalenciasId = new Hashtable <String, String>();
		datosUsuarios = new ArrayList <Identificable>();

		//fUsuarios = new File(Configuracion.get().getProperty("usuarios.nombreFichero"));
		//fEquivalencias = new File(Configuracion.get().getProperty("equivalenciasId.nombreFichero"));

		fUsuarios = new File("usuarios.dat");
		fEquivalencias = new File("equivalId.dat");

		
		//file: bajo nivel, físico  
		//fis: alto nivel, virtual
		recuperarDatos();		
	}
	/**
	 * Recupera el arraylist de objetos almacenados en fichero
	 */
	@Override
	public void recuperarDatos() {
		//Previo a recuperar ->logica de arranque: existe el fichero físico?, si existe se le configura la entrada de datos
		if (fUsuarios.exists() && fEquivalencias.exists() ) {
			try {
				FileInputStream Ufis = new FileInputStream(fUsuarios); //conexion al fichero físico
				ObjectInputStream Uois = new ObjectInputStream(Ufis); //conexion al flujo de entrada				
				datosUsuarios = (List<Identificable>) Uois.readObject(); // necesita tener el objeto del mismo formato en el que se ha guardado

				FileInputStream Efis = new FileInputStream(fEquivalencias); //conexion al fichero físico
				ObjectInputStream Eois = new ObjectInputStream(Efis); //conexion al flujo de entrada				
				equivalenciasId =  (Map<String, String>) Eois.readObject(); // necesita tener el objeto del mismo formato en el que se ha guardado
				return;//esto sale por aqui (devuelve el control) si todo va bien, es preferible al else que interferiría con la lógica del catch
			}
			catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			} 			
		}	
			/*en caso que no salga por el return del lado del if, va a borrar el contenido y dejar los objetos instanciados 
			en blanco para depués cargar los predeterminados
			asegura que vaya a funcionar en caso de que fallen ambos el try Y el if*/
			cargarUsuariosPredeterminados();		 		
	}
	@Override
	public void guardarDatos() {
		//Previo a guardar ->logica de arranque: existe el fichero físico?, si existe se le configura la entrada de datos
		if (fUsuarios.exists() && fEquivalencias.exists() ) {
			try {
				FileOutputStream Ufos = new FileOutputStream(fUsuarios); //conexion al fichero físico
				ObjectOutputStream Uoos = new ObjectOutputStream(Ufos); //conexion al flujo de entrada				
				Uoos.writeObject(datosUsuarios); 

				FileOutputStream Efos = new FileOutputStream(fEquivalencias); //conexion al fichero físico
				ObjectOutputStream Eoos = new ObjectOutputStream(Efos); //conexion al flujo de entrada				
				Eoos.writeObject(equivalenciasId); 
			}
			catch (IOException e) {
				e.printStackTrace();
			} 			
		}	 		
	}	

	@Override
	public void cargarDatos() {
		// TODO Auto-generated method stub

	}

	public static UsuariosDAO getInstance() throws DatosException, ModeloException {
		if (instance == null ) {
			instance = new UsuariosDAO();
		}
		return instance;
	}
	/**
	 * @param clave
	 * @return el id que corresponde a una clave
	 */
	public String getEquivalenciaId(String clave) {
		System.out.println(equivalenciasId.toString());
		return equivalenciasId.get(clave);
	}
	/**
	 * Genera datos de prueba válidos dentro del almacen de datos
	 */
	private void cargarUsuariosPredeterminados() {
		try {
			String nombreUsr = "Admin";
			String password = "Miau#0";
			//			Persona p = new Persona (
			//					);
			Usuario u = new Usuario (new Nif("0000000T"), 
					nombreUsr, "Admin Admin", 
					new DireccionPostal("Luna", "27", "30132", "Murcia"),
					new Correo("admin@gmail.com"), 
					new Fecha(0001, 01, 01), 
					new Fecha(2018, 01, 03), 
					new ClaveAcceso("Miau#0"), 
					RolUsuario.ADMINSTRADOR);

			alta(u);

			nombreUsr = "Invitado";
			password = "Miau#0";

			//			Persona p2 = new Persona (
			//					);
			Usuario inv = new Usuario (new Nif("0000001R"), 
					nombreUsr, "Invi Invi", 
					new DireccionPostal("Luna", "27", "30132", "Murcia"),
					new Correo("invi@gmail.com"), 
					new Fecha(1999, 12, 01), 
					new Fecha(2018, 01, 03), 
					new ClaveAcceso("Miau#0"), 
					RolUsuario.INVITADO);

			alta(inv);
			//			
			//			alta(new Usuario (
			//					new Persona (new Nif("0000001R"), 
			//							nombreUsr, "Invi Invi", 
			//							new DireccionPostal("Luna", "27", "30132", "Murcia"),
			//							new Correo("invi@gmail.com"), 
			//							new Fecha(1999, 12, 01)
			//							),
			//					new Fecha(2018, 01, 03), 
			//					new ClaveAcceso("Miau#0"), 
			//					RolUsuario.INVITADO)
			//					);			
		}
		catch (ModeloException | DatosException e) {			
		}

	}
	@Override
	public int getSize() {
		return datosUsuarios.size();
	}



	/**
	 * Registro de la sesión de usuario.
	 * @param sesionUsuario 
	 * @return 
	 * @throws DatosException 
	 */
	@Override
	public void alta(Object obj) throws DatosException {
		// Registra usuario a partir de la última posición ocupada.
		assertNotNull(obj);
		Usuario usr = (Usuario) obj;

		int posicionInsercion = indexSort(usr.getId(), datosUsuarios);

		if (posicionInsercion < 0) {
			datosUsuarios.add(-posicionInsercion -1, (Identificable) usr);
			registrarEquivalencias(usr);

		} 
		else {
			//Generar variante y comprueba de nuevo
			try {
				usr = new Usuario( usr, usr.getId());
			} catch (ModeloException e) {
				e.printStackTrace();
			}
			posicionInsercion = indexSort(usr.getId(), datosUsuarios);
			// Error identificador ya existe
			throw new DatosException ("Alta usuario ya existe");

		}
	}
	//TODO someday: metodo producir variantes
	//	private void producirVariantesIsUsr( Usuario usr) throws DatosException {
	//		int posInsercion;
	//		// Coincidencia de id: hay que generar variante
	//		int intentos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".length();
	//		//		do {
	//		//			usr = new Usuario(usr);
	//		//			posInsercion = indexSort(usr.getId());
	//		//			
	//		//		}
	//
	//	}

	/**
	 * Muestra por consola todos los usuarios almacenados.
	 */
	public void mostrarDatos() {
		for (Identificable usr: datosUsuarios) {
			if (usr == null) {
				break;
			}
			System.out.println("\n" + usr);			
		}
	}

	private void registrarEquivalencias(Usuario usr) {
		equivalenciasId.put(usr.getNif().getTexto(), usr.getId());
		equivalenciasId.put(usr.getCorreo().getTexto(), usr.getId());
		equivalenciasId.put(usr.getId(), usr.getId());
	}

	@Override
	public Usuario obtener(String id) {

		assertNotNull(id);
		id = equivalenciasId.get(id);

		int posicion = 0;
		if (id != null) {
			posicion = indexSort(id, datosUsuarios) - 1; // base 1
			if (posicion < 0) {
				equivalenciasId.get(id);
				return (Usuario) datosUsuarios.get(posicion-1); // base 1
			}
			return (Usuario) datosUsuarios.get(posicion);
		}		
		return null;
	}
	@Override
	public List obtenerTodos() {		
		return datosUsuarios;
	}
	@Override
	public Object baja(String id) throws DatosException {	
		assertNotNull(id);
		int posicion = indexSort(id, datosUsuarios); 
		if (posicion <0) {
			Usuario usrAborrar = (Usuario) datosUsuarios.get(posicion-1);
			equivalenciasId.remove(usrAborrar.getId());
			equivalenciasId.remove(usrAborrar.getNif().getTexto());
			equivalenciasId.remove(usrAborrar.getCorreo().getTexto());
			return usrAborrar;
		}
		else {
			throw new DatosException("Baja " + id + " no existe");
		}

	}
	
	/**
	 * Actualizar datos de n Usuario reemplazando el almacenado por el que se recibe como param
	 * No admite cambios en el idUsr
	 * @param obj
	 * @throws DatosException 
	 */
	@Override
	public void actualizar(Object obj) throws DatosException {
		Usuario usrActualizado = (Usuario)obj;
		int posicion = indexSort(usrActualizado.getId(), datosUsuarios);
		if (posicion <0) {
			// reemplaza equivalencias de nif y correo
			Usuario usrModificado = (Usuario) datosUsuarios.get(posicion-1);
			equivalenciasId.remove(usrModificado.getNif().getTexto());
			equivalenciasId.remove(usrModificado.getCorreo().getTexto());
			equivalenciasId.put(usrActualizado.getNif().getTexto(), usrActualizado.getId());
			equivalenciasId.put(usrActualizado.getCorreo().getTexto(), usrActualizado.getId());
			//reemplaza elemento
			datosUsuarios.set(posicion -1, usrActualizado); //to base 0
		}
		else {
			throw new DatosException("actualizar: "+ usrActualizado.getId() + " no existe");
		}

	}
	@Override
	public String listarDatos() {
		StringBuilder result = new StringBuilder();

		for (Identificable usr: datosUsuarios) {
			if (usr == null) {
				break;
			}
			System.out.println("\n" + usr);			
		}
		return result.toString();
	}
	@Override
	public String listarId() {
		StringBuilder result = new StringBuilder();

		for (Identificable usr: datosUsuarios) {
			result.append("\n" + usr.getId());
			System.out.println("\n" + usr);			
		}
		return result.toString();
	}

	@Override
	public void borrarTodo() {
		datosUsuarios.clear();
		equivalenciasId.clear();
		cargarUsuariosPredeterminados();
	}
	@Override
	public void cerrar() {		
		guardarDatos();
	}



}
