/** 
Proyecto: Juego de la vida.
 * Implementa la fachada de datos
 * @since: prototipo2.0
 * @source: Datos.java 
 * @version: 1.26 - 21.3.19
 * @coauthor: ajp 
 * @coauthor: dam18-23jpr 4004010
 */

package accesoDatos;

import static org.junit.Assert.assertNotNull;
//en este import, según tenga puesto fichero o memoria, se puede elegir donde queremos que actue la fachada
import accesoDatos.fichero.*;
import modelo.*;

public class Datos {
	
	private UsuariosDAO usuariosDAO;
	private SesionesUsrDAO sesionesUsrDAO;
	private SimulacionesDAO simulacionesDAO;
	private MundosDAO mundosDAO;
	
	
	public Datos() throws DatosException, ModeloException {
		usuariosDAO = UsuariosDAO.getInstance();
		sesionesUsrDAO = SesionesUsrDAO.getInstance(); 
		simulacionesDAO = SimulacionesDAO.getInstance();
		mundosDAO = MundosDAO.getInstance();
	}

	/**
	 * @param clave
	 * @return el id que corresponde a una clave
	 */
	public String getEquivalenciaId(String clave) {
		return usuariosDAO.getEquivalenciaId(clave);
	}
	public int getSesionesRegistradas() {
		return sesionesUsrDAO.getSize();
	}
	public int getUsuariosRegistrados() {
		return usuariosDAO.getSize();
	}
	public int getSimulacionesRegistradas() {
		return simulacionesDAO.getSize();
	}
	public int getMundosRegistrados() {
		return mundosDAO.getSize();
	}
	/**
	 * Cerrar almacén de datos desde fachada
	 */
	public void cerrar() {
		usuariosDAO.cerrar();
		sesionesUsrDAO.cerrar();
		usuariosDAO.cerrar();
		simulacionesDAO.cerrar();
		mundosDAO.cerrar();
	}
	/**
	 * buscar usuario por su id, base 1
	 * @param id
	 * @return Usuario
	 * TODO: arreglar esto 
	 */
	public Usuario buscarUsuario(String id) {
		
		return (usuariosDAO.obtener(id));
	}

	/**
	 * Registro de la sesión de usuario.
	 * @param sesionUsuario 
	 * @throws DatosException 
	 */
	public void altaUsuario(Usuario usr) throws DatosException {
		usuariosDAO.alta(usr);
	}

	/**
	 * Muestra por consola todos los usuarios almacenados.
	 */
	public String toStringDatosUsuarios() {
		return usuariosDAO.listarDatos();
	}

//	/**
//	 * Genera datos de prueba válidos dentro 
//	 * del almacén de datos.
//	 */
//	public void cargarUsuariosPrueba() {	
//		for (int i = 0; i < 10; i++) {
//			try {
//				altaUsuario(new Usuario
//								(new Persona 
//										( new Nif("0000000" + i + "TRWAGMYFPDXBNJZSQVHLCKE".charAt(i)),
//										"Pepe", "López Pérez",
//										new DireccionPostal("Luna", "27", "30132", "Murcia"),
//										new Correo("pepe" + i + "@gmail.com"), 
//										new Fecha(1999, 11, 12)
//										), 
//								new Fecha(2018, 01, 03), 
//								new ClaveAcceso("Miau#" + i), RolUsuario.NORMAL)
//								);
//			} 
//			catch (ModeloException | DatosException e ) {				
//			}
//		}
//	}

	/**
	 * Registro de la sesión de usuario.
	 * @param sesionUsuario 
	 * @throws DatosException 
	 */
	public void altaSesion(SesionUsuario sesion) throws DatosException {
		assertNotNull(sesion);
		sesionesUsrDAO.alta(sesion);
	}

	/**
	 * Registro de la simulación.
	 * @param simulacion 
	 * @throws DatosException 
	 */
	public void altaSimulacion(Simulacion simulacion) throws DatosException {
		assertNotNull(simulacion);
		simulacionesDAO.alta(simulacion);
		//datosSimulaciones.add(simulacion);  
	}	

	public Simulacion getSimulacion(String id) {
		// TODO Auto-generated method stub		
		return (Simulacion) simulacionesDAO.obtener(id);
	}	

} // class
