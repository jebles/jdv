package accesoDatos.memoria;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import modelo.Correo;
import modelo.DireccionPostal;
import modelo.Identificable;
import modelo.ModeloException;
import modelo.SesionUsuario;
import modelo.Usuario;

public class SesionesUsrDAO extends DAOIndexSort implements OperacionesDAO {

	private List<Identificable> datosSesiones;
	private static SesionesUsrDAO instance;


	//att:contructor privado parausar dentro de la clase para inicializar las movidas 
	private SesionesUsrDAO() {
		datosSesiones = new ArrayList <Identificable>();
	}

	public static SesionesUsrDAO getInstance() {
		if (instance == null ) {
			instance = new SesionesUsrDAO();
		}
		return instance;
	}

	public int getSize() {
		return datosSesiones.size();
	}

	/**
	 * Registro de la sesión de usuario.
	 * @param sesionUsuario 
	 * @throws DatosException 
	 */
	@Override
	public void alta(Object obj) throws DatosException {
		// Registra usuario a partir de la última posición ocupada.
		assert obj != null;
		SesionUsuario sesionNueva = (SesionUsuario) obj;

		int posicionInsercion = indexSort((sesionNueva).getIdSesion(), datosSesiones);

		if (posicionInsercion < 0) {
			datosSesiones.add(-posicionInsercion -1, (Identificable) sesionNueva);
		} 
		else {				
			throw new DatosException ("Alta sesion ya existe");

			// Coincidencia de id: hay que generar variante -> TODO	

		}
	}		

	/**
	 * Muestra por consola todos los usuarios almacenados.
	 */
	public void mostrarDatos() {
		for (Identificable usr: datosSesiones) {
			if (usr == null) {
				break;
			}
			System.out.println("\n" + usr);			
		}
	}	
	//TODO:revisar lógica
	@Override
	public SesionUsuario obtener(String id) {
		assertNotNull(id);
		SesionUsuario sesion;
		
		int posicion=0;;
		
		if (id != null) {
			posicion = indexSort(id, datosSesiones) - 1; // base 1
			if (posicion < 0) {
				datosSesiones.get(posicion);
			}
			return (SesionUsuario) datosSesiones.get(posicion); // base 1
		}
		return null;		
		
	}
	@Override
	public List obtenerTodos() {		
		return datosSesiones;
	}
	//TODO:implementar baja, no está bien como abajo
	
	
//	@Override
//	public SesionUsuario baja(String id) throws DatosException {
//		SesionUsuario sesion_obtenida;
//		//TODO sesion_obtenida
//
//		int posicion = indexSort(sesion_obtenida.getIdSesion(), datosSesiones);
//		if (posicion <0) {
//			SesionUsuario sesionAborrar = (SesionUsuario) datosSesiones.get(posicion-1);			
//			datosSesiones.remove(sesionAborrar);
//		}
//		else {
//			throw new DatosException();
//		}
//		return sesion_obtenida;
//	}
	/**
	 * Actualizar datos de n Usuario reemplazando el almacenado por el que se recibe como param
	 * No admite cambios en el idUsr
	 * @param obj
	 * @throws DatosException //TODO
	 */
//	@Override
//	public void actualizar(Object obj) throws DatosException {
//		SesionUsuario usrActualizado = (SesionUsuario)obj;
//		int posicion = indexSort(usrActualizado.getIdSesion(), datosSesiones);
//		if (posicion < 0) {
//			// reemplaza equivalencias de nif y correo
//			Usuario usrModificado = (Usuario) datosSesiones.get(posicion-1);
//			datosSesiones.remove(usrModificado.getNif().getTexto());
//			equivalenciasId.remove(usrModificado.getCorreo().getTexto());
//			equivalenciasId.put(usrActualizado.getNif().getTexto(), usrActualizado.getId());
//			equivalenciasId.put(usrActualizado.getCorreo().getTexto(), usrActualizado.getId());
//			//reemplaza elemento
//			datosSesiones.set(posicion -1, usrActualizado); //to base 0
//		}
//		else {
//			throw new DatosException("actualizar: "+ usrActualizado.getId() + " no existe");
////		}
//
//	}
	@Override
	public String listarDatos() {
		StringBuilder result = new StringBuilder();

		for (Identificable usr: datosSesiones) {//TODO
			if (usr == null) {
				break;
			}
			System.out.println("\n" + usr);			
		}
		return result.toString();
	}
	@Override
	public String listarId() {
		StringBuilder result = new StringBuilder();

		for (Identificable usr: datosSesiones) {//TODO
			result.append("\n" + usr.getId());
			System.out.println("\n" + usr);			
		}
		return result.toString();
	}

	@Override
	public Object baja(String id) throws DatosException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void actualizar(Object obj) throws DatosException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void borrarTodo() {
		// TODO Auto-generated method stub
		
	}



}



