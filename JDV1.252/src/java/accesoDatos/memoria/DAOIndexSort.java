/**
 * Clase abastacta para generlaizar el uso del método de búsqueda binaria usada en los DAO
 * Para obtener los identificadores de los tipos diferentes de objetos de cada lista,
 * se apoya en la interfaz Identificable
 * @author ajp
 * @author jpr 4004010
 * @since 2.0
 * @date 26.3.19
 */
package accesoDatos.memoria;

import java.util.List;

import modelo.Identificable;

public abstract class DAOIndexSort {
	
	/**
	 * Busca usuario dado su id.
	 * El indice es en base 1
	 * basado en busqueda binaria
	 * @param id - el id del Usuario a buscar.
	 * @return - el Usuario encontrado o el indice negativo de su posición teórica
	 */
	protected int indexSort(String id, List <Identificable> datos) { //ver posibilidad de tener datos como variable de la clase
		int comparacion = 0;
		int medio = 0;
		int ini = 0;
		int fin = datos.size() - 1;

		while(ini <= fin){
			medio = (fin + ini) / 2;

			comparacion = id.compareTo((datos.get(medio)).getId());

			if(comparacion == 0) {
				return medio + 1;
			}
			if (comparacion > 0) {
				ini = medio + 1;
			}
			else {
				fin = medio - 1;
			}
		}
		return -(ini + 1);
	}	


}
