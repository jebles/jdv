package configuracion;

import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;

public class Configuracion {
	

	/** 
	 * 	Proyecto: Juego de la vida.
	 *  Gestión de la configuracion del proyecto mediante el uso de java properties
	 *  @since: prototipo2.0
	 *  @source: Configuracion.java
	 *  @version: 2.0 - 14.3.19
	 *  @author: jpr
	 */

	    private static Properties propiedades;

	    private Configuracion() {
	        propiedades = new Properties();
	        FileInputStream is = null;	   

	        try {
	            is = new FileInputStream("src/java/configuracion/con.cfg");
	            propiedades.load(is);

	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	    }

	    public static Properties get() {
	        if (propiedades == null) {
	            new Configuracion();
	        }
	        return propiedades;
	    }
	}

