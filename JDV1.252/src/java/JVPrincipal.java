/** 
Proyecto: Juego de la vida.
 * Implementa el control de inicio de sesión y ejecución de la simulación por defecto. 
 * @since: prototipo1.0
 * @source: JVPrincipal.java 
 * @version: 1.2 - 14.2.19
 * @author: ajp
 * @author: jpr 4004010
 */

import accesoDatos.Datos;
import accesoDatos.DatosException;
import accesoUsr.Presentacion;
import modelo.ModeloException;
import modelo.SesionUsuario;
import modelo.Simulacion;
import util.Fecha;

public class JVPrincipal {

	private static Datos datos;
	private static Presentacion interfazUsr;	
	
	/**
	 * Secuencia principal del programa.
	 */
	public static void main(String[] args) {		
				
		try {
			JVPrincipal jv = new JVPrincipal();
			//instanciar fachada (Datos)
			jv.datos = new Datos();
			jv.interfazUsr = new Presentacion();			
			//jv.datos.cargarUsuariosPrueba();		
			System.out.println(jv.datos.toStringDatosUsuarios());
			//TODO : aplicacion.datos.cargarMundoDemo();

			if (jv.interfazUsr.inicioSesionCorrecto()) {
				SesionUsuario sesion = new SesionUsuario();
				sesion.setUsr(interfazUsr.getUsrEnSesion());
				sesion.setFecha(new Fecha());  // Hoy
				
				jv.datos.altaSesion(sesion);	
				
				System.out.println("Sesión: " + datos.getSesionesRegistradas() + '\n' + "Iniciada por: " 
						+ jv.interfazUsr.getUsrEnSesion().getNombre() + " " 
						+ jv.interfazUsr.getUsrEnSesion().getApellidos());	
				
				jv.interfazUsr.mostrarSimulacion();
			}
			else {
				System.out.println("\nDemasiados intentos fallidos...");
			}
			jv.datos.cerrar();
			System.out.println("Fin del programa.");
		} catch (ModeloException | DatosException e) {
			e.printStackTrace();
		}
	}

} //class
