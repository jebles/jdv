/** 
 * Proyecto: Juego de la vida.
 * Organiza aspectos de gestión de la simulación según el modelo1.2 
 * @since: prototipo1.0
 * @source: Simulacion.java 
 * @version: 1.1 - 28.3.19
 * @author: ajp
 * @coauthor jpr
 */

package modelo;

import accesoUsr.Presentacion;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.Serializable;

import accesoDatos.DatosException;
import util.Fecha;

public class Simulacion  implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum EstadoSimulacion {PREPARADA, INICIADA, COMPLETADA}
	private static final int CICLOS_SIMULACION = 20;

	private Usuario usr;
	private Fecha fecha;
	private Mundo mundo;
	private EstadoSimulacion estado;

	/**
	 * Constructor convencional. Establece el valor inicial de cada uno de los
	 * atributos. Recibe parámetros que se corresponden con los atributos. Utiliza
	 * métodos set... para la posible verificación.
	 * @param usr
	 * @param fecha
	 * @param mundo
	 * @param estado
	 */
	public Simulacion(Usuario usr, Fecha fecha, Mundo mundo, EstadoSimulacion estado) {
		setUsr(usr);
		setFecha(fecha);
		setMundo(mundo);
		setEstado(estado);
		leyesEstandar();
	}

	public void setEstado(EstadoSimulacion est) {
		assertNotNull(est);
		estado = est;		
	}

	/**
	 * Constructor por defecto. Establece el valor inicial, por defecto, de cada uno
	 * de los atributos. Llama al constructor convencional de la propia clase.
	 * @throws DatosException 
	 */
	public Simulacion() throws ModeloException {
		this(new Usuario(), new Fecha(), new Mundo(), EstadoSimulacion.PREPARADA);
		leyesEstandar();
	}

	/**
	 * Constructor copia. Establece el valor inicial de cada uno de los atributos a
	 * partir de los valores obtenidos de un objeto de su misma clase. El objeto
	 * Usuario es compartido (agregación). Llama al constructor convencional.
	 * @param s - la Simulacion a clonar
	 */
	public Simulacion(Simulacion s) {
		this(s.usr, s.fecha.clone(), s.mundo, s.estado);
	}

	public Usuario getUsr() {
		return usr;
	}

	public void setUsr(Usuario usr) {
		assert usr != null;
		this.usr = usr;
	}

	public Mundo getMundo() {
		return mundo;
	}
	
	public void setMundo(Mundo mundo) {
		assert mundo != null;
		this.mundo = mundo;
	}

	public Fecha getFecha() {
		return fecha;
	}

	public void setFecha(Fecha fecha) {
		assert fecha != null;
		this.fecha = fecha;
	}
	private void leyesEstandar() {
		mundo.constantes.put("ValoresSobrevivir", new int[] { 2, 3 });
		mundo.constantes.put("ValoresRenacer", new int[] { 3 });
	}

	public static int getCiclosSimulacion() {
		return CICLOS_SIMULACION;
	}

	public String getIdSimulacion() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getUsr().getId());
		sb.append(this.fecha.getMarcaTiempoMilisegundos());
		return sb.toString();
	}

	@Override
	public String toString() {
		return String.format("Simulacion [usr=%s, fecha=%s, mundo=%s]", usr, fecha, mundo);
	}

	/**
	 * Ejecuta una simulación del juego de la vida en la consola.
	 * @throws ModeloException 
	 * @throws DatosException 
	 */
	public void lanzarDemo() throws DatosException, ModeloException {

		int generacion = 0;

		do {
			System.out.println("\nGeneración: " + generacion);
			new Presentacion().mostrarSimulacion();
			mundo.actualizarMundo();
			generacion++;
		} while (generacion < CICLOS_SIMULACION);
	}
	@Override
	public boolean equals (Object obj) {
		if (obj != null && getClass() == obj.getClass()) {
			if (this == obj) {
				return true;
			}
			if (usr.equals(((Simulacion)obj).usr) &&
					fecha.equals(((Simulacion)obj).fecha) &&
					mundo.equals(((Simulacion)obj).mundo) &&
					estado.equals(((Simulacion)obj).estado)
					) {
				return true;
			}					
		}
		return false;		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((mundo == null) ? 0 : mundo.hashCode());
		result = prime * result + ((usr == null) ? 0 : usr.hashCode());
		return result;
	}
	
	
} //class
