	/** Proyecto: Juego de la vida.
 *  Implementa el concepto de Usuario según el modelo1.
 *  En esta versión se ha revisado el diseño OO previo.
 *  @since: prototipo1.0
 *  @source: Usuario.java 
 *  @version: 1.1 - 2019/01/21 
 *  @author: ajp
 */

package modelo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import configuracion.Configuracion;
import util.Fecha;


public class Usuario extends Persona implements Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public enum RolUsuario {INVITADO, NORMAL, ADMINSTRADOR};

	private String id;
	private Fecha fechaAlta;
	private ClaveAcceso claveAcceso;
	private RolUsuario rol;

	
	/**
	 * Constructor convencional. Utiliza métodos set...()
	 * @param fechaAlta
	 * @param claveAcceso
	 * @param rol
	 * @throws ModeloException 
	 */
	public Usuario(Nif nif, String nombre, String apellidos,
			DireccionPostal domicilio, Correo correo, 
			Fecha fechaNacimiento, Fecha fechaAlta, 
			ClaveAcceso claveAcceso, RolUsuario rol) 
			throws ModeloException {
		super( nif, nombre, apellidos, domicilio, correo, fechaNacimiento);	
		
		setFechaAlta(fechaAlta);
		setClaveAcceso(claveAcceso);
		setRol(rol);
		generarIdUsr();
	}

	/**
	 * Constructor por defecto. Reenvía al constructor convencional.
	 * @throws ModeloException 
	 */
	public Usuario() throws ModeloException {
		this(new Nif(), "Algo", "Algomas Arro", new DireccionPostal(), new Correo(), 
				new Fecha().addAños(-Integer.parseInt(Configuracion.get().getProperty("usuario.edadMinima"))), 
				new Fecha(),	
				new ClaveAcceso(), 
				RolUsuario.NORMAL
				);
	}

	/**
	 * TODO: check persona
	 * Constructor copia.
	 * @param usr
	 * @throws ModeloException 
	 */
	public Usuario(Usuario usr) throws ModeloException {
		
		this.fechaAlta = new Fecha(usr.fechaAlta.getAño(), 
				usr.fechaAlta.getMes(), usr.fechaAlta.getDia());
		this.claveAcceso = new ClaveAcceso(usr.claveAcceso);
		this.rol = usr.rol;
	}
	
	public Usuario(Usuario usr, String id) throws ModeloException{
		Persona person = new Persona(usr);	
		this.setPersona(person);

		this.fechaAlta = new Fecha(usr.fechaAlta.getAño(), 
				usr.fechaAlta.getMes(), usr.fechaAlta.getDia());
		this.claveAcceso = new ClaveAcceso(usr.claveAcceso);
		this.rol = usr.rol;
		this.id = id;
		}
	/**
	 * Genera un identificador sintético a partir de:
	 * La letra inicial del nombre, 
	 * Las dos iniciales del primer y segundo apellido,
	 * Los dos último caracteres del nif.
	 * @return id
	 */
	public String getId() {
		return this.id;
	}
	/**
	 * Metodo que genera un ID de usuario
	 * @param id
	 * @return id
	 */
	public void generarIdUsr() {
		StringBuilder id = new StringBuilder();
		id.append(this.nombre.substring(0, 1).toUpperCase());
		String[]divApellidos = this.apellidos.split("\\s+");
		id.append(divApellidos[0].substring(0, 1).toUpperCase());
		id.append(divApellidos[1].substring(0, 1).toUpperCase());
		id.append(this.nif.getTexto().substring(7, 9));
		this.id = id.toString();
		//return id.toString();
}
	
	/**
	 * Genera una variante cambiando la última letra del id 
	 * por la siguiente en el alfabeto previsto para el nif.
	 * @param id
	 * @return nuevo id
	 */
	private void generarVarianteIdUsr() {
		String alfabetoNif = "ABCDEFGHJKLMNPQRSTUVWXYZ";
		String alfabetoNifDesplazado = "BCDEFGHJKLMNPQRSTUVWXYZA";
		this.id =  this.id.substring(0, 4) 
				+ alfabetoNifDesplazado.charAt(alfabetoNif.indexOf(id.charAt(4)));
	}
	public void setPersona(Persona pax) {
		assertNotNull(pax);
		//super(pax);
	}
	public Persona getPersona(Persona pax) {
		return pax;
	}	

	public Fecha getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Fecha fechaAlta) throws ModeloException {
		assert fechaAlta != null;
		if (fechaAltaValida(fechaAlta)) {
			this.fechaAlta = fechaAlta;
		}
		else {
			throw new ModeloException("Fecha de alta incorrecta");
		}
	}

	/**
	 * Comprueba validez de una fecha de alta.
	 * @param fechaAlta.
	 * @return true si cumple.
	 */
	private boolean fechaAltaValida(Fecha fechaAlta) {
		return !fechaAlta.after(new Fecha()); 	
	}

	public ClaveAcceso getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(ClaveAcceso claveAcceso) {
		assert claveAcceso != null;
		this.claveAcceso = claveAcceso;
	}

	public RolUsuario getRol() {
		return rol;
	}

	public void setRol(RolUsuario rol) {
		assert	rol != null;
		this.rol = rol;
	}
	

	/**
	 * Redefine el método heredado de la clase Objecto.
	 * @return el texto formateado del estado -valores de atributos- de objeto de la clase Usuario.  
	 */
	@Override
	public String toString() {
		return String.format(
				"%-16s %s\n"		
				+ "%-16s %s\n"
				+ "%-16s %s\n"
				+ "%-16s %s\n", 		
					"fechaAlta:", this.fechaAlta.getAño() + "." 
				+ this.fechaAlta.getMes() + "." 
				+ this.fechaAlta.getDia(), 
					"claveAcceso:", this.claveAcceso, 
					"rol:", this.rol,
					"IDENT: ", this.getId()
				);		
	}

} // class

