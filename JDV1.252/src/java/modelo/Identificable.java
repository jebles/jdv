/**
 * interfaz para obtener el Id del tipo de objeto concreto para apoyar la búsqueda binaria general
 * @since 2.0
 * @author dam18-23 4004010
 * @author ajp
 * @date 26.3.19
 */

package modelo;

public interface Identificable {
	
	public String getId();
	//TODO: implantar cambios en resto de DAO
}
