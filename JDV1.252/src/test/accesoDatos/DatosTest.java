package accesoDatos;
/**
 * En el subpaquete test.accesoDatos hay que crear una clase 
 * llamada DatosTest donde se ubicarán todos los métodos 
 * relacionados con la preparación y configuración de los 
 * datos necesarios para las pruebas durante el desarrollo 
 * del proyecto en su prototipo2. Varios métodos de estos ya 
 * existían desde el prototipo1 en la clase Datos, pero ahora 
 * quedan mejor organizados
 * DAM1 
 * @author: dam18-23 - 4004010
 * @since: 2.0
 * @source: DatosTest.java
 * @version: 21 mar. 2019
 */
public class DatosTest {

}
