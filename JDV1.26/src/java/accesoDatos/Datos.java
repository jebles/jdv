/** 
Proyecto: Juego de la vida.
 * Implementa el almacén de datos del programa. 
 * @since: prototipo1.1
 * @source: Datos.java 
 * @version: 1.26 - 14.2.19
 * @coauthor: ajp 
 * @coauthor: dam18-23jpr 4004010
 */

package accesoDatos;

import java.util.ArrayList;
import java.util.HashMap;
import modelo.*;
import modelo.Usuario.RolUsuario;
import util.Fecha;

public class Datos {

	// entr < > ponemos lo que queremos que se asegure que el contenido sean objetos usuario (~infer)
	private static ArrayList <Usuario> datosUsuarios = new ArrayList <Usuario>();
	private static ArrayList <SesionUsuario> datosSesiones = new ArrayList <SesionUsuario>();
	private static ArrayList <Simulacion> datosSimulaciones = new ArrayList <Simulacion>();
	private static ArrayList <Mundo> datosMundo = new ArrayList <Mundo>();
	private static HashMap<String, String> mapaEquivalencias = new HashMap<String, String>(); 

	/**
	 * @param clave
	 * @return el id que corresponde a una clave
	 */
	public String getEquivalenciaId(String clave) {
		return mapaEquivalencias.get(clave);
	}
	public int getSesionesRegistradas() {
		return datosSesiones.size();
	}
	public int getUsuariosRegistrados() {
		return datosUsuarios.size();
	}
	public int getSimulacionesRegistradas() {
		return datosSimulaciones.size();
	}

	/**
	 * buscarusuario por su id, base 1
	 * @param id
	 * @return Usuario
	 * TODO: arreglar esto 
	 */
	public Usuario buscarUsuario(String id) {
		assert id != null;
	//	id = equivalenciasId.get(id);
		
		int posicion;
		if (id != null) {
			posicion = indexSort(id) - 1; // base 1
			if (posicion < 0) {
				mapaEquivalencias.get(id);
			}			
		}		
		return datosUsuarios.get(indexSort(id) - 1 ); // base 1

	}
	/**
	 * Busca usuario dado su id.
	 * El indice es en base 1
	 * basado en busqueda binaria
	 * @param idUsr - el id del Usuario a buscar.
	 * @return - el Usuario encontrado o el indice negativo de su posición teórica
	 */
	public int indexSort(String idUsr) {		

		int medio = 0;
		int inf = 0;
		int sup = datosUsuarios.size() - 1;

		while(inf <= sup){
			medio = (sup + inf) / 2;
			
			int comparacion = idUsr.compareTo(datosUsuarios.get(medio).getIdUsr());
			
			if(comparacion == 0) {
				return medio + 1;
			}
			if (comparacion > 0) {
				inf = medio + 1;
			}
			else {
				sup = medio - 1;
			}
		}
		return -(inf + 1);
	}	

	/**
	 * Registro de la sesión de usuario.
	 * @param sesionUsuario 
	 */
	public void altaUsuario(Usuario usr) {
		// Registra usuario a partir de la última posición ocupada.
		assert usr != null;
		int posicionInsercion = indexSort(usr.getIdUsr());

		if (posicionInsercion < 0) {
			datosUsuarios.add(-posicionInsercion -1, usr);
			registrarEquivalencias(usr);

		} else {
			//Generar variante y comprueba de nuevo
			usr = new Usuario( usr, usr.getIdUsr());
			posicionInsercion = indexSort(usr.getIdUsr());
			// Error identificador ya existe
			throw new DatosException ("Alta usuario ya existe");
			// Coincidencia de id: hay que generar variante			
			
		}
	}

	/**
	 * Muestra por consola todos los usuarios almacenados.
	 */
	public void mostrarTodosUsuarios() {
		for (Usuario usr: datosUsuarios) {
			if (usr == null) {
				break;
			}
			System.out.println("\n" + usr);			
		}
	}

	/**
	 * Genera datos de prueba válidos dentro 
	 * del almacén de datos.
	 */
	public void cargarUsuariosPrueba() {	
		for (int i = 0; i < 10; i++) {
			try {
				altaUsuario(new Usuario(new Nif("0000000" + i + "TRWAGMYFPDXBNJZSQVHLCKE".charAt(i)), 
						"Pepe", "López Pérez", 
						new DireccionPostal("Luna", "27", "30132", "Murcia"),
						new Correo("pepe" + i + "@gmail.com"), 
						new Fecha(1999, 11, 12), 
						new Fecha(2018, 01, 03), 
						new ClaveAcceso("Miau#" + i), RolUsuario.NORMAL));
			} 
			catch (ModeloException e ) {				
			}
		}
	}

	/**
	 * Registro de la sesión de usuario.
	 * @param sesionUsuario 
	 */
	public void altaSesion(SesionUsuario sesion) {
		datosSesiones.add(sesion);  
	}

	/**
	 * Registro de la simulación.
	 * @param simulacion 
	 */
	public void altaSimulacion(Simulacion simulacion) {
		datosSimulaciones.add(simulacion);  
	}
	/**
	 * Registro de las equivalencias de la tabla nif-id de usuarios
	 * @param usuario 
	 */

	private void registrarEquivalencias(Usuario usr) {
		mapaEquivalencias.put(usr.getNif().getTexto(), usr.getIdUsr());
		mapaEquivalencias.put(usr.getCorreo().getTexto(), usr.getIdUsr());
		mapaEquivalencias.put(usr.getIdUsr(), usr.getIdUsr());
	}

} // class
