	/** Proyecto: Juego de la vida.
 *  Implementa el concepto de Usuario según el modelo1.
 *  En esta versión se ha revisado el diseño OO previo.
 *  @since: prototipo1.0
 *  @source: Usuario.java 
 *  @version: 1.1 - 2019/01/21 
 *  @author: ajp
 */

package modelo;

import util.Fecha;


public class Usuario {
	
	public enum RolUsuario {INVITADO, NORMAL, ADMINSTRADOR};
	
	static final int EDAD_MINIMA = 16;
	private Nif nif;
	private String nombre;
	private String apellidos;
	private String idUsr;
	private DireccionPostal domicilio;
	private Correo correo;
	private Fecha fechaNacimiento;
	private Fecha fechaAlta;
	private ClaveAcceso claveAcceso;
	private RolUsuario rol;

	
	/**
	 * Constructor convencional. Utiliza métodos set...()
	 * @param nif
	 * @param nombre
	 * @param apellidos
	 * @param domicilio
	 * @param correo
	 * @param fechaNacimiento
	 * @param fechaAlta
	 * @param claveAcceso
	 * @param rol
	 * @throws DatosException 
	 */
	public Usuario(Nif nif, String nombre, String apellidos,
			DireccionPostal domicilio, Correo correo, Fecha fechaNacimiento,
			Fecha fechaAlta, ClaveAcceso claveAcceso, RolUsuario rol) 
			throws ModeloException {
		
		setNif(nif);
		setNombre(nombre);
		setApellidos(apellidos);
		setDomicilio(domicilio);
		setCorreo(correo);
		setFechaNacimiento(fechaNacimiento);
		setFechaAlta(fechaAlta);
		setClaveAcceso(claveAcceso);
		setRol(rol);
		generarIdUsr();
	}

	/**
	 * Constructor por defecto. Reenvía al constructor convencional.
	 * @throws DatosException 
	 */
	public Usuario() throws ModeloException {
		this(new Nif(), 
				"Nombre", 
				"Apellido Apellido", 
				new DireccionPostal(),
				new Correo(), 
				new Fecha().addAños(-EDAD_MINIMA), 
				new Fecha(), 
				new ClaveAcceso(), 
				RolUsuario.NORMAL);
	}

	/**
	 * Constructor copia.
	 * @param usr
	 */
	public Usuario(Usuario usr) {
		this.nif = new Nif(usr.nif);
		this.nombre = new String(usr.nombre);
		this.apellidos = new String(usr.apellidos);
		this.domicilio = new DireccionPostal(usr.domicilio);
		this.correo = new Correo(usr.correo);
		this.fechaNacimiento = new Fecha(usr.fechaNacimiento.getAño(), 
				usr.fechaNacimiento.getMes(), usr.fechaNacimiento.getDia());
		this.fechaAlta = new Fecha(usr.fechaAlta.getAño(), 
				usr.fechaAlta.getMes(), usr.fechaAlta.getDia());
		this.claveAcceso = new ClaveAcceso(usr.claveAcceso);
		this.rol = usr.rol;
	}
	public Usuario(Usuario usr, String usrId) {
		//TODO
	}

	/**
	 * Genera un identificador sintético a partir de:
	 * La letra inicial del nombre, 
	 * Las dos iniciales del primer y segundo apellido,
	 * Los dos último caracteres del nif.
	 * @return idUsr
	 */
	public String getIdUsr() {
		assert this.nif != null;
		assert this.nombre != null;
		assert this.apellidos != null;
		String[] apellidos = this.apellidos.split(" ");
		return this.nombre.charAt(0) 
				+ apellidos[0].charAt(0) + apellidos[1].charAt(0)
				+ this.nif.getTexto().substring(7);
	}
	/**
	 * Metodo que genera un ID de usuario
	 * @param idUsr
	 * @return id
	 */
	public String generarIdUsr() {
		StringBuilder id = new StringBuilder();
		id.append(this.nombre.substring(0, 1).toUpperCase());
		String[]divApellidos = this.apellidos.split("\\s+");
		id.append(divApellidos[0].substring(0, 1).toUpperCase());
		id.append(divApellidos[1].substring(0, 1).toUpperCase());
		id.append(this.nif.getTexto().substring(7, 9));
		this.idUsr = id.toString();
		return id.toString();
}
	
	/**
	 * Genera una variante cambiando la última letra del idUsr 
	 * por la siguiente en el alfabeto previsto para el nif.
	 * @param idUsr
	 * @return nuevo idUsr
	 */
	private void generarVarianteIdUsr() {
		String alfabetoNif = "ABCDEFGHJKLMNPQRSTUVWXYZ";
		String alfabetoNifDesplazado = "BCDEFGHJKLMNPQRSTUVWXYZA";
		this.idUsr =  this.idUsr.substring(0, 4) 
				+ alfabetoNifDesplazado.charAt(alfabetoNif.indexOf(idUsr.charAt(4)));
	}
	
	public Nif getNif() {
		return nif;
	}

	public void setNif(Nif nif) {
		assert nif != null;	
		this.nif = nif;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) throws ModeloException {	
		assert nombre != null;
		if (nombreValido(nombre)) {
			this.nombre = nombre;
		}
		else {
			throw new ModeloException("Formato de nombre del usuario no válido");
		}
	}

	/**
	 * Comprueba validez del nombre.
	 * @param nombre.
	 * @return true si cumple.
	 */
	private boolean nombreValido(String nombre) {
		return nombre.matches("^[A-ZÑÁÉÍÓÚ][A-ZÑÁÉÍÓÚa-zñáéíóúü ]+");
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) throws ModeloException {
		assert apellidos != null;
		if (apellidosValidos(apellidos)) {
			this.apellidos = apellidos;
		}
		else {
			throw new ModeloException("Formato de apellidos del usuario no válido");
		}
	}

	/**
	 * Comprueba validez de los apellidos.
	 * @param apellidos.
	 * @return true si cumple.
	 */
	private boolean apellidosValidos(String apellidos) {
		return apellidos.matches("^[A-ZÑÁÉÍÓÚ][A-ZÑÁÉÍÓÚa-zñáéíóúü ]*");
	}

	public DireccionPostal getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(DireccionPostal domicilio) {
		assert domicilio != null;
		this.domicilio = domicilio;
	}

	public Correo getCorreo() {
		return correo;
	}

	public void setCorreo(Correo correo) {
		assert correo != null;
			this.correo = correo;
	}

	public Fecha getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Fecha fechaNacimiento) throws ModeloException {
		assert fechaNacimiento != null;
		if (fechaNacimientoValida(fechaNacimiento)) {
			this.fechaNacimiento = fechaNacimiento;
		}
		else {
			throw new ModeloException("Fecha de nacimiento incorrecta");
		}
	}

	/**
	 * Comprueba validez de una fecha de nacimiento.
	 * @param fechaNacimiento.
	 * @return true si cumple.
	 */
	private boolean fechaNacimientoValida(Fecha fechaNacimiento) {
		return !fechaNacimiento.after(new Fecha().addAños(-EDAD_MINIMA));
	}

	public Fecha getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Fecha fechaAlta) throws ModeloException {
		assert fechaAlta != null;
		if (fechaAltaValida(fechaAlta)) {
			this.fechaAlta = fechaAlta;
		}
		else {
			throw new ModeloException("Fecha de alta incorrecta");
		}
	}

	/**
	 * Comprueba validez de una fecha de alta.
	 * @param fechaAlta.
	 * @return true si cumple.
	 */
	private boolean fechaAltaValida(Fecha fechaAlta) {
		return !fechaAlta.after(new Fecha()); 	
	}

	public ClaveAcceso getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(ClaveAcceso claveAcceso) {
		assert claveAcceso != null;
		this.claveAcceso = claveAcceso;
	}

	public RolUsuario getRol() {
		return rol;
	}

	public void setRol(RolUsuario rol) {
		assert	rol != null;
		this.rol = rol;
	}
	

	/**
	 * Redefine el método heredado de la clase Objecto.
	 * @return el texto formateado del estado -valores de atributos- de objeto de la clase Usuario.  
	 */
	@Override
	public String toString() {
		return String.format(
				"%-16s %s\n"
		+ "%-16s %s\n"
		+ "%-16s %s\n"
		+ "%-16s %s\n"
		+ "%-16s %s\n"
		+ "%-16s %s\n"
		+ "%-16s %s\n"
		+ "%-16s %s\n"
		+ "%-16s %s\n", 
		"nif:", nif, 
		"nombre:", this.nombre, 
		"apellidos:", this.apellidos,  
		"domicilio:", this.domicilio, 
		"correo:", this.correo, 
		"fechaNacimiento:", this.fechaNacimiento.getAño() + "." 
				+ this.fechaNacimiento.getMes() + "." 
				+ this.fechaNacimiento.getDia(),	
		"fechaAlta:", this.fechaAlta.getAño() + "." 
				+ this.fechaAlta.getMes() + "." 
				+ this.fechaAlta.getDia(), 
		"claveAcceso:", this.claveAcceso, 
		"rol:", this.rol
		);		
	}

} // class

