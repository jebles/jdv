/** 
 *  Proyecto: Juego de la vida.
 *  Excepcion para la gestión de errores de Modelo  
 *  @since: prototipo1.2
 *  @source: ModeloException.java 
 *  @version: 1.25 - 5.3.19 
 *  @author: ajp
 *  @coauthor: 4004010
 */
package modelo;

public class ModeloException extends Exception {

	public ModeloException(String mensaje) {
		super(mensaje);
	}
	public ModeloException() {
		super();
	}	

} //class
