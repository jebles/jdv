/** 
Proyecto: Juego de la vida.
 * Implementa el control de inicio de sesión y ejecución de la simulación por defecto. 
 * @since: prototipo1.0
 * @source: JVPrincipal.java 
 * @version: 1.2 - 14.2.19
 * @author: ajp
 * @author: jpr 4004010
 */

import accesoDatos.Datos;
import accesoUsr.Presentacion;
import modelo.ModeloException;
import modelo.SesionUsuario;
import modelo.Simulacion;
import util.Fecha;

public class JVPrincipal {

	private static Datos datos;
	private static Presentacion interfazUsr;
	
	
	/**
	 * Secuencia principal del programa.
	 */
	public static void main(String[] args) {		
				
		try {
			datos = new Datos();
			interfazUsr = new Presentacion();
			
			datos.cargarUsuariosPrueba();		
			datos.mostrarTodosUsuarios();

			if (interfazUsr.inicioSesionCorrecto()) {
				SesionUsuario sesion = new SesionUsuario();
				sesion.setUsr(interfazUsr.getUsrEnSesion());
				sesion.setFecha(new Fecha());  // Hoy
				
				datos.altaSesion(sesion);	
				
				System.out.println("Sesión: " + datos.getSesionesRegistradas() + '\n' + "Iniciada por: " 
						+ interfazUsr.getUsrEnSesion().getNombre() + " " 
						+ interfazUsr.getUsrEnSesion().getApellidos());	
				
				new Simulacion().lanzarDemo();
			}
			else {
				System.out.println("\nDemasiados intentos fallidos...");
			}		
			System.out.println("Fin del programa.");
		} catch (ModeloException e) {
			e.printStackTrace();
		}
	}

} //class
